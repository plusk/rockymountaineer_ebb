;( function( window ) {

  'use strict';
    
  /**
   * Extend obj function
   *
   * This is an object extender function. It allows us to extend an object
   * by passing in additional variables and overwriting the defaults.
   */
  function extend( a, b ) {
    for( var key in b ) { 
      if( b.hasOwnProperty( key ) ) {
        a[key] = b[key];
      }
    }
    return a;
  }
    

  /**
   * PKTextSplitter
   */
  function PKIMGSplitter( options ) {
    // function body...
     this.options = extend( {}, this.options );
     extend( this.options, options ); 
     this._init(); 
  }
    
   /**
   * PKTextSplitter options Object
   *
   * @type {HTMLElement} wrapper - The wrapper to create the ray in.
   */
  PKTextSplitter.prototype.options = {
      wrapper : document.body,
      direction : 'up'
  }

  PKTextSplitter.prototype._init = function() {
      // create element
      this.el = this.options.wrapper;
      // ASSUMES that img is the first node!
      this.img = this.el.children[0];
      console.log(this.img);
      
      this.words = [];
      this.pos = [];
      
      var pBounds = this.img.getBoundingClientRect();
      var buffer = 50;

      for (var i=1;i<this.el.children.length;i++){
          var dimg = this.img.cloneNode();
          this.words[i-1] = this.el.children[i];
          
          this.words[i-1].style.overflow = 'hidden';
          var bounds = this.words[i-1].getBoundingClientRect();
          this.pos[i-1] = { width:bounds.width, height:bounds.height };
          this.pos[i-1].left = bounds.left - pBounds.left;
          this.pos[i-1].top = bounds.top - pBounds.top;
          
          dimg.style.left = (-this.pos[i-1].left ) +'px';
          dimg.style.top = ( -this.pos[i-1].top ) +'px';
          
          // todo. depending on direction. will need to reset this.
          //this.words[i-1].style.top = ( this.pos[i-1].top + this.pos[i-1].height ) +'px';
          // use transform for smoother animation.
          var offset = this.pos[i-1].height;
          TweenLite.set(this.words[i-1],{y:offset,height:0});
          
          this.words[i-1].appendChild(dimg);
      }
      
      TweenLite.set(this.img,{display:'none'});
    };  
    
    /**
     * PKTextSplitter show
     *
     * This function starts our text demasking
     * assumes GSAP.
     */
    PKTextSplitter.prototype.show = function( obj ) {
        // {time:.5, ease:Back.easeOut, delay:0, delayIncrement:1/20, secondStage:3, secondStageOffset:1}
        
        var delay = obj.delay;
        for (var i=0;i<this.words.length;i++){
            TweenLite.to( this.words[i], obj.time, {delay:delay,y:0,height:this.pos[i].height,ease:obj.ease} );
            delay += obj.delayIncrement;
        }
    }
    PKTextSplitter.prototype.reset = function() {
        var buffer = 50;
        for (var i=0;i<this.words.length;i++){
            var offset = this.pos[i].height;
            TweenLite.killTweensOf( this.words[i] );
            TweenLite.set( this.words[i], {y:offset,height:0} );
        }
    }
    
  /**
   * Add to global namespace
   */
  window.PKTextSplitter = PKTextSplitter;

})( window );